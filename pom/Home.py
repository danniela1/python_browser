import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import unittest

class Home(unittest.TestCase):
    #constructor
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 30)
        self.search_word = "q"
    #method that asks the user for a word
    def test_enter_search(self):
        self.wait.until(ec.visibility_of_element_located((By.NAME, self.search_word)))
        search = input("what're you searching for? ")
        self.driver.find_element_by_name(self.search_word).send_keys(search)
    #method that clicks 'enter' in search textbox
    def test_enter_results(self):
        self.driver.find_element_by_name(self.search_word).send_keys(Keys.ENTER)


