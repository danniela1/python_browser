import unittest
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import  WebDriverWait
class SearchPage(unittest.TestCase):

    #constructor
    def __init__(self, driver):
        self.driver = driver
        self.suggestion_className = "gL9Hy"
        self.click_suggestion = "//a[@class='gL9Hy']"
        self.description_className = "st"
        self.search_word = "q"
    #method that validates if there's a suggestion in the search
    def test_validation_search(self):
        self.wait = WebDriverWait(self.driver, 30)
        self.wait.until(ec.visibility_of_element_located((By.NAME, self.search_word)))
        try:
            self.driver.find_element_by_class_name(self.suggestion_className).is_enabled()
            self.driver.find_element_by_xpath(self.click_suggestion).click()
            print("suggestion")
        except:
            print("no suggestion")
    # method that validates the page title with the previous search
    def test_validation_title(self):
        self.wait.until(ec.visibility_of_element_located((By.NAME, self.search_word)))
        try:
            word = self.driver.find_element_by_name(self.search_word).text
            if self.driver.title.__contains__(word):
                print("valid title")
        except:
            print("invalid title")
    #method that validates all the descriptions in the results page
    def test_validation_description(self):
        self.wait.until(ec.visibility_of_element_located((By.NAME, self.search_word)))
        try:
            if self.driver.find_element_by_class_name(self.description_className).is_displayed():
                print("all results have description")
        except:
            print("no description in results")