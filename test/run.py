from selenium import webdriver
import time
import unittest
from pom.Home import Home
from pom.SearchPage import SearchPage

class Test_home(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="C:\\Users\\nnavarrete\\PycharmProjects\\Browser\\driver\\chromedriver.exe")
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_homeGoogle(self):
        driver = self.driver
        driver.get("https://www.google.com/")
        titleOfPage = driver.title
        self.assertEqual("Google", titleOfPage, "titles are not the same")

    def test_search(self):
        home = Home(self.driver)
        home.test_enter_search()
        home.test_enter_results()
        
        self.assertTrue("element enabled", self.driver.find_element_by_class_name(home.search_word).is_displayed())


    def test_validations(self):
        search = SearchPage(self.driver)
        search.test_validation_search()
        search.test_validation_description()
        search.test_validation_title()
        
        self.assertTrue("element enabled", self.driver.find_element_by_class_name(search.description_className).is_displayed())
        self.assertTrue("element enabled", self.driver.find_element_by_class_name(search.search_word).is_displayed())

    @classmethod
    def tearDownClass(cls):
        time.sleep(2)
        cls.driver.close()
        cls.driver.quit()
        print("test completed")


if __name__ == "__main__":
    unittest.main()
